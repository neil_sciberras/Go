Please note that the source code was partially implemented (just a skeleton to act as a guide) 
by the tutor, then the implementation work is done by me. 

Part 2 of the assignment revolves around Concurrent Go Programming.

The following functions are implemented in seq.go:

	Succ(in, out chan int)	
	Sink(in chan int)	
	Tail(in, out chan int)	
	Prefix(n int, in, out chan int)	
	DeltaWG(in, out_x, out_y chan int) and Delta(in, out_x, out_y chan int)
	Nos(out chan int)
	Int(in, out chan int)
	Pairs(in, out chan int)	
	Fib(out chan int)
	Squares(out chan int)

