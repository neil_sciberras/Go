/// ----------------------------------------------------------------------------
/// @author Duncan Paul Attard
///
/// Model answers for worksheet 1 questions.
/// ----------------------------------------------------------------------------
package util

// -----------------------------------------------------------------------------
// Computes factorial numbers using direct recursion.
// func Fact(n) where:
//   * n is the number whose factorial is to be computed.
// Returns: The factorial for index n.
// -----------------------------------------------------------------------------
func Fact(n int) int {
	//Direct recursion creates a chain of dependencies, in that the caller has 
	//to wait for the called recursive function to return back, for the original 
	//caller to finish its computation and return its value to its own caller. 
	if n == 0 {
		return 1
	} else {
		return Fact(n-1)*n
	}
}

// -----------------------------------------------------------------------------
// Computes factorial numbers using indirect recursion.
// func Fact2(n) where:
//   * n is the number whose factorial is to be computed.
// Returns: The factorial for index n.
// -----------------------------------------------------------------------------
func Fact2(n int) int {
  return Fact3(n, 1)
}

func Fact3(n int, acc int) int {
	//Indirect recursion/Tail recursion, computes the necessary computation and 
	//passes it as an argument to any further recursive calls it has to make. 
	//In this way, no recursion unwinding has to be made, and no chain of 
	//dependencies is created. 
	if n == 0 {
		return acc
	} else {
		return Fact3(n-1, acc*n)
	}
}

// -----------------------------------------------------------------------------
// Computes fibonacci numbers using direct recursion.
// func Fib(n) where:
//   * n is the number whose fibonacci number is to be computed.
// Returns: The fibonacci number for index n.
// -----------------------------------------------------------------------------
func Fib(n int) int {
	//We are just using the switch with no condition to use it for multiple if 
	//else statements. 
	switch {
	case n == 0:
		return 0
	case n == 1:
		return 1
	default:
		return Fib(n-1)+Fib(n-2)
	}
}

// -----------------------------------------------------------------------------
// Computes fibonacci numbers using indirect recursion.
// func Fib2(n) where:
//   * n is the number whose fibonacci number is to be computed.
// Returns: The fibonacci number for index n.
// -----------------------------------------------------------------------------
func Fib2(n int) int {
  return Fib3(n, 0, 1)
}

func Fib3(n int, n_minus2 int, n_minus1 int) int {
	switch {
	case n == 0:
		return n_minus2
	default:
		return Fib3(n-1, n_minus1, n_minus2+n_minus1)
	}
}

//The indexing of the fibonacci sequence starts from 0 not 1. 
// Thus  Number index    : [0, 1, 2, 3, 4, 5, ...]
// 		Fibonacci number: [1, 1, 2, 3, 5, 8, ...]

// EXAMPLE
//	fib2(6)
//	= fib3(7,0,1)
//	= fib3(6,1,1)
//	= fib3(5,1,2)
//	= fib3(4,2,3)
//	= fib3(3,3,5)
//	= fib3(2,5,8)
//	= fib3(1,8,13)
//	= fib3(0,13,21)
//	= 13



// -----------------------------------------------------------------------------
// Maps an array of elements into another array of elements having equal length.
// func Map(f, elems) where:
//   * f is a function that maps the current element x in the array to a new
//     value.
//   * elems is the array to map.
// Returns: The same array with f applied on each of its elements.
// -----------------------------------------------------------------------------
func Map(f func(int) int, elems []int) []int {
	
	//This creates a zeroed array of length equal to that of the 'elems' slice 
	//and returns the slice that refers to it. 
	felems := make([]int, len(elems))
	
	//The range iterates over the slice 'elelms' and returns the index 'i'
	//and the value 'v' for every iteration.
	for i, v := range elems {
		//For every index in elems array/slice we are applying the function 'f' on 
		//that value and putting it into the new slice/array 'felems'. 
		felems[i] = f(v)
	}
	
	return felems
}

// -----------------------------------------------------------------------------
// Filters an array of elements according to the specifed predicate f.
// func Filter(f, elems) where:
//   * f is a function that tests the current element x in the array and returns
//     a boolean, indicating whether x is to be retained.
//   * elems is the array to filter.
// Returns: A new array filtered according to f, possibly containing fewer
//          elements than the original array.
// -----------------------------------------------------------------------------
func Filter(f func(int) bool, elems []int) []int {
	
	//A nil slice, with length and capacity of zero. When an element needs to be 
	//added to the slice (retuirns true when f is applied on it), we append to this slice.
	var filtered []int
	
	//We are using the blank identifier in the multiple assignment part, since
	//the index returned by the range is not used, so we tell the compiler that 
	//we will not be using it by using the blank identifier.
	for _, v := range elems {
	
		//If the current element in the original array 'v' returns true when 
		//'f' is applied to it, then we want to append it to the 'filtered' 
		//slice/array.
		if f(v) == true {
			//Appending the value in 'elems' to the slice using the 'append' function.
			filtered = append(filtered, v)
			//fmt.Println(i, " ", v, " passed the test")
		}
	}
	
	return filtered
}

// -----------------------------------------------------------------------------
// Reduces an array of elements using the specified combining function f,
// starting with the seed value.
// func Reduce(f, seed, elems) where:
//   * f is a function that combines the current element x in the array with the
//      next element x + 1 in the array.
//   * seed is the seed that the reduction starts with.
//   * elems is the array to reduce.
// Returns: A single value that reflects the reduction of elems.
// -----------------------------------------------------------------------------
func Reduce(f func(x int, acc int) int, seed int, elems []int) int {
	//Same as foldl, since it starts reducing (applying the function) from the 
	//left most elements.
	
	//This is the base case. 
	//When the slice has no elements in it (length zero), then we return the 
	//seed value. 
	if len(elems) == 0 {
		return seed
		
	//When there are elements in the slice, we apply function 'f' on the first 
	//element and the seed, and pass the return value of the function as the new 
	//seed of the new recursive Reduce call.
	//We pass the 'tail' opf the slice =, by using the [1:] which re-slices the 
	//slice, and takes from elements 1 to the last element in the previous slice.
	} else {
		return Reduce(f, f(elems[0], seed), elems[1:])
	}
}

// -----------------------------------------------------------------------------
// Reverses the specified list.
// func Reverse(elems) where:
//   * elems is the array to be reversed.
// Returns: A new array that is the reverse of elems.
// -----------------------------------------------------------------------------
func Reverse(elems []int) []int {
	
	//This is the base case, for when the slice to reverse is empty, we just 
	//return that same slice.
	if len(elems) == 0 {
		return elems
		
	//When the slice is non-empty, we append the first element in the slice 
	//to the slice returned by the recursive call of function Reverse on the 
	//tail of the original slice. 
	//The expression 'elems[1:]' will always represent the tail of the slice 
	//passed as argument to the function Reverse.
	} else {
		return append(Reverse(elems[1:]), elems[0])
	}
}
