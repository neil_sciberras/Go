Please note that the source code was partially implemented (just a skeleton to act as a guide) 
by the tutor, then the implementation work is done by me. 

Part 1 of the assignment revolves around Sequential Go Programming.


The following functions are implemented in seq.go:
		
		Fact(n int) int 
	computes factorial numbers using direct recursion, where n is the number
	whose factorial is to be returned.
	
		Fact2(n int) int 
		Fact3(n int, acc int) int
	compute factorial numbers using indirect recursion.

	
		Fib(n int) int 
	computes fibonacci numbers using direct recursion, where n is the number
	whose Fibonacci number is to be returned.
	
		Fib2(n int) int
		Fib3(n int, n_minus2 int, n_minus1 int) int 
	computes Fibonacci numbers using indirect recursion.

		Map(f func(int) int, elems []int) []int 
	maps an array of elements into another array of elements having equal length,
	where f is a function that maps the current element x in the array to a new 
	value and elems is the array to map.

		Filter(f func(int) bool, elems []int) []int 
	filters an array of elements according to the specifed predicate f, where f 
	is a function that tests the current element x in the array and returns a 
	boolean, indicating whether x is to be retained, and elems is the array to filter.

		Reduce(f func(x int, acc int) int, seed int, elems []int) int 
	reduces an array of elements using the specified combining function f, 
	starting with the seed value, where f is a function that combines the current 
	element x in the array with the next element x + 1 in the array, seed is the
	seed that the reduction starts with, and elems is the array to reduce.

		Reverse(elems []int) []int 
	reverses the specified list.